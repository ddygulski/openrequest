<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190710175758 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE idea_idea_category_list (idea_id INT NOT NULL, idea_category_list_id INT NOT NULL, INDEX IDX_5FC90AF15B6FEF7D (idea_id), INDEX IDX_5FC90AF13E2B87C5 (idea_category_list_id), PRIMARY KEY(idea_id, idea_category_list_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE idea_idea_category_list ADD CONSTRAINT FK_5FC90AF15B6FEF7D FOREIGN KEY (idea_id) REFERENCES idea (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE idea_idea_category_list ADD CONSTRAINT FK_5FC90AF13E2B87C5 FOREIGN KEY (idea_category_list_id) REFERENCES idea_category_list (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user CHANGE current_avatar current_avatar INT DEFAULT NULL, CHANGE roles roles JSON NOT NULL');
        $this->addSql('ALTER TABLE idea DROP categories, CHANGE short_description short_description VARCHAR(500) DEFAULT NULL, CHANGE description description VARCHAR(4000) DEFAULT NULL, CHANGE goals goals VARCHAR(4000) DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE idea_idea_category_list');
        $this->addSql('ALTER TABLE idea ADD categories LONGTEXT DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci COMMENT \'(DC2Type:array)\', CHANGE short_description short_description VARCHAR(500) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci, CHANGE description description VARCHAR(4000) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci, CHANGE goals goals VARCHAR(4000) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE user CHANGE roles roles LONGTEXT NOT NULL COLLATE utf8mb4_bin, CHANGE current_avatar current_avatar INT DEFAULT NULL');
    }
}
